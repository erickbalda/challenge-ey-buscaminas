import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuscaminasComponent } from './components/buscaminas/buscaminas.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/buscaminas'
      },
      {
        path: 'buscaminas',
        component: BuscaminasComponent
      }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
