import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscaminasComponent } from './buscaminas.component';
import { AppModule } from 'src/app/app.module';

describe('BuscaminasComponent', () => {
  let component: BuscaminasComponent;
  let fixture: ComponentFixture<BuscaminasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscaminasComponent ],
      imports: [AppModule],

    })
    .compileComponents();

    fixture = TestBed.createComponent(BuscaminasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Titulo de primera columna', () => {
    const fixture = TestBed.createComponent(BuscaminasComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.firstColumn h1').textContent).toContain('Buscaminas');
  });

  it('Titulo de segunda columna', () => {
    const fixture = TestBed.createComponent(BuscaminasComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.secondColumn h1').textContent).toContain('Marcador');
  });


});
