import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { Casilla } from 'src/app/interface/casilla';
import { Marcador } from 'src/app/interface/marcador';
import { Tablero } from 'src/app/interface/tablero';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-root',
    templateUrl: './buscaminas.component.html',
    styleUrls: ['./buscaminas.component.css']
})
export class BuscaminasComponent implements OnInit, AfterViewInit {
    title = "EY Front Challenge -Erick Balda";
    @ViewChild(MatTable) table!: MatTable<any>;
    @ViewChild(MatSort) sort!: MatSort;
    @ViewChild(MatPaginator) paginator!: MatPaginator;

    juegoCompletado = false;
    tablero: Tablero = {
        dimensionX: 10,
        dimensionY: 10,
        nroMinas: 10
    };
    contadorMinas!: number;
    minasDetectadas: number = 0;
    casillasDescubiertas: number = 0;
    contadorTiempo!: number;

    casillasTablero: Casilla[][] = [];
    alrededorCasilla = [
        { posX: -1, posY: -1 },
        { posX: -1, posY: 0 },
        { posX: -1, posY: 1 },
        { posX: 0, posY: 1 },
        { posX: 0, posY: -1 },
        { posX: 1, posY: -1 },
        { posX: 1, posY: 0 },
        { posX: 1, posY: 1 }
    ];
    dataSource = new MatTableDataSource<Marcador>();
    marcadorColumns: string[] = ['id', 'resultado', 'tiempo'];

    constructor(
        private _snackBar: MatSnackBar
    ) { };

    /**
     * Carga marcador del localStorage
     * Inicializa tablero
     * Inicia contador
     */
    ngOnInit() {
        let a = localStorage.getItem('marcador');
        this.dataSource.data = a ? JSON.parse(a) : {};
        this.crearTablero(this.tablero);
        this.contadorTiempo = Date.now();
    }

    /**
     * Inicializa tabla de marcadores
     */
    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    /**
     * Reinicia el tablero a su estado inicial para una nueva partida
     */
    reiniciarTablero() {
        this.crearTablero(this.tablero);
        this.contadorTiempo = Date.now();
    }

    /**
     * Genera la grilla de casillas 
     * @param tablero 
     */
    crearTablero(tablero: Tablero) {
        this.casillasTablero = [];
        //Llena tablero
        for (let x = 0; x < tablero.dimensionX; x++) {
            this.casillasTablero.push([]);
            for (let y = 0; y < tablero.dimensionY; y++) {
                this.casillasTablero[x].push(
                    {
                        posicionX: x,
                        posicionY: y,
                        descubierta: false,
                        posibleMina: false,
                        esMina: false,
                        minasProximas: 0,
                        icono: ''
                    });
            }
        }
        // Coloca Minas
        for (let i = 0; i < tablero.nroMinas; i++) {
            let casillaMina = this.casillasTablero[this.posicionAleatoria(tablero.dimensionX)][this.posicionAleatoria(tablero.dimensionY)];
            while (casillaMina.esMina) {
                casillaMina = this.casillasTablero[this.posicionAleatoria(tablero.dimensionX)][this.posicionAleatoria(tablero.dimensionY)];
            }
            casillaMina.esMina = true;

        }

        // Cuenta Minas
        for (let x = 0; x < tablero.dimensionX; x++) {
            for (let y = 0; y < tablero.dimensionY; y++) {
                this.casillasTablero[x][y].minasProximas = this.minasAlrededor(x, y);
            }
        }
        this.contadorMinas = tablero.nroMinas;
        this.juegoCompletado = false;
        this.minasDetectadas = 0;
        this.casillasDescubiertas = 0;
    }


    /**
     * Contador de minas alrededor de una casilla
     * @param x posicion horizontal 
     * @param y posicion vertical
     * @returns numero de minas alrededor de una casilla
     */
    minasAlrededor(posX: number, posY: number): number {
        let nroMinasProximas = 0;
        this.alrededorCasilla.forEach(element => {
            nroMinasProximas = nroMinasProximas + (this.checkEsMina(posX + element.posX, posY + element.posY))
        });
        return nroMinasProximas;
    }

    /**
     * Chequea si una casilla posee una mina
     * @param posX posicion Horizontal
     * @param posY posicion Vertical
     * @returns Valor 1 si la casilla posee mina
     */
    checkEsMina(posX: number, posY: number): number {
        let enlimiteTablero = (posX >= 0 && posY >= 0) && (posX < this.tablero.dimensionX && posY < this.tablero.dimensionY);
        let esMina = 0;
        if (enlimiteTablero) {
            esMina = this.casillasTablero[posX][posY].esMina ? 1 : 0;
        }
        return esMina;
    }

    /**
     * Chequea el valor de una casilla al ser descubierta
     * @param casilla datos de la casilla a verificar
     * @returns
     */
    chequeaCasilla(casilla: Casilla) {
        if (this.juegoCompletado || casilla.descubierta || casilla.posibleMina)
            return;

        casilla.descubierta = true;
        this.casillasDescubiertas += 1;
        if (casilla.esMina) {
            casilla.icono = 'M'
            this.contadorMinas -= 1;
            this.juegoCompletado = true;
            this.registrarMarcador('Fallido')
            return;
        } else if (casilla.minasProximas === 0) {
            let casillasAlrededor = this.casillasVacias(casilla);
            if (casillasAlrededor.length > 0) {

                for (let casillaAlrededor of casillasAlrededor) {
                    this.chequeaCasilla(casillaAlrededor);
                }
            }
        }
        casilla.icono = casilla.minasProximas.toString();
        this.chequearTablero();
    }

    /**
     * Coloca bandera donde se sospeche hay una mina
     * @param casilla datos de la casilla a verificar
     * @param event 
     * @returns 
     */
    colocarBandera(casilla: Casilla, event: any) {
        event.preventDefault();
        if (this.juegoCompletado || casilla.descubierta) {
            return;
        }

        if (this.contadorMinas === 0 && !casilla.esMina
            && this.casillasDescubiertas === (this.tablero.dimensionX * this.tablero.dimensionY - this.minasDetectadas)
            && this.minasDetectadas === this.tablero.nroMinas) {
            this.juegoCompletado = true;
            return;
        }

        if (this.minasDetectadas >= 0 && this.minasDetectadas < this.tablero.nroMinas ||
            casilla.posibleMina && this.minasDetectadas === this.tablero.nroMinas) {
            casilla.icono = casilla.posibleMina ? '' : '!';
            this.minasDetectadas += casilla.posibleMina ? -1 : 1;
            casilla.posibleMina = !casilla.posibleMina;
            this.contadorMinas += casilla.posibleMina ? -1 : 1;
            this.chequearTablero();
        }
    }

    /**
     * Revisa casillas que no posean minas o minas alrededor
     * @param casilla datos de la casilla a verificar
     * @returns retorna array con las casillas que no posean minas o minas alrededor
     */
    casillasVacias(casilla: Casilla): Casilla[] {
        let casillasVacias: any[] = [];
        for (let pos of this.alrededorCasilla) {
            let enlimiteTablero = ((casilla.posicionX + pos.posX) >= 0 && (casilla.posicionY + pos.posY) >= 0)
                && ((casilla.posicionX + pos.posX) < this.tablero.dimensionX && (casilla.posicionY + pos.posY) < this.tablero.dimensionY);
            if (!enlimiteTablero)
                continue;
            let casillaAlrededor = this.casillasTablero[casilla.posicionX + pos.posX][casilla.posicionY + pos.posY];
            if (!casillaAlrededor.esMina && !casillaAlrededor.descubierta) {
                casillasVacias.push(casillaAlrededor);
            };
        }
        return casillasVacias;
    }

    /**
     * Registra resultado de la partida
     * @param resultado texto resultado de la partida
     */
    registrarMarcador(resultado: string) {
        this.openSnackBar(resultado);
        this.dataSource.data.push({ id: this.dataSource.data.length + 1, resultado: resultado, tiempo: Date.now() - this.contadorTiempo })
        localStorage.setItem('marcador', JSON.stringify(this.dataSource.data));
        this.paginator._changePageSize(this.paginator.pageSize);
        this.sort.direction = 'desc';
        this.dataSource.sort = this.sort;
    }

    /**
     * Revisa si se ha completado la partida
     * @returns 
     */
    chequearTablero() {
        let tableroCompletado = this.casillasDescubiertas === ((this.tablero.dimensionX * this.tablero.dimensionY) - this.contadorMinas - this.minasDetectadas);
        let minasEncontradas = this.contadorMinas + this.minasDetectadas === this.tablero.nroMinas;
        if (tableroCompletado && minasEncontradas) {
            this.juegoCompletado = true;
            this.registrarMarcador('Resuelto');
        }
    }

    /**
     * Abre un snack con el resultado de la partida
     * @param message Mensaje para notificacion
     */
    openSnackBar(message: string) {
        this._snackBar.open(message, 'Cerrar', {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 5000,
        });
    }

    /**
     * Retorna posicion aleatoria del 0 al numero maximo parametro
     * @param max valor limite (excluido)
     * @returns valor aleatorio entre o y el numero limite(excluido)
     */
    posicionAleatoria(max: number): number {
        return Math.floor(Math.random() * max);
    }

}

