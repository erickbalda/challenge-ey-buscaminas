export interface Casilla {    
    posicionX: number;
    posicionY: number;
    descubierta: boolean
    posibleMina: boolean
    esMina: boolean;
    minasProximas: number;
    icono: string
}
